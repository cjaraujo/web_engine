<?php

namespace Modulos\Distribuidor;

use \Core\Http\Request\Request;
use \Core\Http\Response\Response;

/**
 * Serviço do Distribuidor
 * @author marcos
 */
class DistribuidorService
{

    public function executar($request, $response)
    {
        $response->setConteudo('<html><body><h1>Hello world!</h1></body></html>');
        $response->setStatusCode(Response::HTTP_OK);
        $response->getHeaders()->set('Content-Type', 'text/html');
    }
}