<?php
namespace Modulos\Distribuidor\Repositorio;

/**
 * Description of Repositorio
 *
 * @author cleoj
 */
class Repositorio extends \Core\Repositorio\ARepositorio{
    protected $tabela = 'distribuidores';
    protected $prefixo = 'di_';
    protected $chavePrimaria = 'id';
    protected $campos = ['id','nome'];
    protected $classeEntidade = Entidade::class;
    protected $classeColecao = Colecao::class;
}
