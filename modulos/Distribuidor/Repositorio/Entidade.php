<?php
namespace Modulos\Distribuidor\Repositorio;

/**
 * Description of Entidade
 *
 * @author cleoj
 */
class Entidade extends \Core\Repositorio\Entidade{
    protected $campos = ['id','nome'];
    protected $classeRepositorio = Repositorio::class;
}
