<?php

namespace Modulos\Distribuidor\Repositorio;

/**
 * Description of Colecao
 *
 * @author cleoj
 */
class Colecao extends \Core\Repositorio\Colecao{
    protected $campos = ['id','nome'];
}
