<?php
namespace Modulos\Distribuidor\Migracao;

/**
 * Description of Teste
 *
 * @author cleoj
 */
class Teste extends \Core\Migracao\Abstrato\AMigracao{
    public function atualizar() {
        \Core\BancoDeDados\BancoDeDados::get()
                ->inserir('teste',['nome'=>'marcuzim']);
    }

    public function reverter() {
        
    }

}
