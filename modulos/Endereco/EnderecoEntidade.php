<?php

namespace Modulos\Endereco;

/**
 * Entidade de Endereço
 * @author marcos
 */
class EnderecoEntidade
{
    private $pais;
    private $estado;
    private $cidade;
    private $bairro;
    private $cep;
    private $endereco;
    private $complemento;
    private $numero;

    public function __construct($pais, $estado, $cidade, $bairro, $cep,
                                $endereco, $complemento, $numero)
    {
        $this->pais        = $pais;
        $this->estado      = $estado;
        $this->cidade      = $cidade;
        $this->bairro      = $bairro;
        $this->cep         = $cep;
        $this->endereco    = $endereco;
        $this->complemento = $complemento;
        $this->numero      = $numero;
    }

    public function getPais()
    {
        return $this->pais;
    }

    public function getEstado()
    {
        return $this->estado;
    }

    public function getCidade()
    {
        return $this->cidade;
    }

    public function getBairro()
    {
        return $this->bairro;
    }

    public function getCep()
    {
        return $this->cep;
    }

    public function getEndereco()
    {
        return $this->endereco;
    }

    public function getComplemento()
    {
        return $this->complemento;
    }

    public function getNumero()
    {
        return $this->numero;
    }
}