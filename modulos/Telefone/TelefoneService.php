<?php

namespace Modulos\Telefone;

/**
 * Description of TelefoneService
 * @author marcos
 */
class TelefoneService
{
    /**
     * Cadastra uma entidade de telefone
     * @param \Modulos\Telefone\TelefoneEntidade $telefone
     */
    public function novo(TelefoneEntidade $telefone){

    }

    /**
     * Atualiza uma entidade de telefone
     * @param \Modulos\Telefone\TelefoneEntidade $telefone
     */
    public function atualizar(TelefoneEntidade $telefone){

    }

    /**
     * Remove uma entidade de telefone
     * @param \Modulos\Telefone\TelefoneEntidade $telefone
     */
    public function remover(TelefoneEntidade $telefone){

    }
}