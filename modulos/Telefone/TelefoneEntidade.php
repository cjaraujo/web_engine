<?php

namespace Modulos\Telefone;

/**
 * Entidade de Telefone
 * @author marcos
 */
class TelefoneEntidade
{
    private $id;
    private $operadora;
    private $codigoDeArea;
    private $ddd;
    private $numero;

    public function __construct(int $id, string $operadora, int $codigoDeArea, int $ddd, string $numero)
    {
        $this->id           = $id;
        $this->operadora    = $operadora;
        $this->codigoDeArea = $codigoDeArea;
        $this->ddd          = $ddd;
        $this->numero       = $numero;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getOperadora()
    {
        return $this->operadora;
    }

    public function getCodigoDeArea()
    {
        return $this->codigoDeArea;
    }

    public function getDdd()
    {
        return $this->ddd;
    }

    public function getNumero()
    {
        return $this->numero;
    }
}