<?php

namespace Modulos\XFactor;

/**
 * Gene base para a criação de um módulo.
 * @author marcos
 */
abstract class XAbstractClass
{

    public static function version(): float
    {
        return 0.1;
    }
}