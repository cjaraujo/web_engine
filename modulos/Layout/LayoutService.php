<?php

namespace Modulos\Layout;

use \Modulos\XFactor\XAbstractClass;
use \Modulos\XFactor\XInterface;

/**
 * Deficinção de layouts da aplicação
 * @author marcos
 */
class LayoutService extends XAbstractClass implements XInterface
{

    public static function getLayout(float $idLayout): string
    {
        $request = \Core\Request::getRequestUri();
        return "<h1>blá blá blá layout id: {$idLayout} - uri {$request}</h1>";
    }
}