<?php

namespace Modulos\Pessoa;

/**
 * Entidade de Pessoa
 * @author marcos
 */
class PessoaEntidade
{
    private $nome;
    private $sexo;
    private $estadoCivil;
    private $email;
    private $dependentes;
    private $tipoPessoa;
    private $dataNascimento;
    private $nacionalidade;
    private $naturalidade;

    public function __construct(string $nome, int $sexo,int $estadoCivil,string $email,
                                int $dependentes, int $tipoPessoa, $dataNascimento,
                                int $nacionalidade, int $naturalidade)
    {
        $this->nome           = $nome;
        $this->sexo           = $sexo;
        $this->estadoCivil    = $estadoCivil;
        $this->email          = $email;
        $this->dependentes    = $dependentes;
        $this->tipoPessoa     = $tipoPessoa;
        $this->dataNascimento = $dataNascimento;
        $this->nacionalidade  = $nacionalidade;
        $this->naturalidade   = $naturalidade;
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function getSexo()
    {
        return $this->sexo;
    }

    public function getEstadoCivil()
    {
        return $this->estadoCivil;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getDependentes()
    {
        return $this->dependentes;
    }

    public function getTipoPessoa()
    {
        return $this->tipoPessoa;
    }

    public function getDataNascimento()
    {
        return $this->dataNascimento;
    }

    public function getNacionalidade()
    {
        return $this->nacionalidade;
    }

    public function getNaturalidade()
    {
        return $this->naturalidade;
    }
}