<?php
use \Core\Http\Response\Response;
use \Core\Http\Request\Request;
use \Core\Router; 

require_once('../vendor/autoload.php');
chdir ('../');
Core\Bootstrap::start(__DIR__);
Core\Router::get('/auth', '\Core\OAuth2\AutorizationServer@executar');
Core\Router::get('/migracao', '\Core\Migracao\Migracao@executar');
Core\Router::get('/distribuidor', '\Modulos\Distribuidor\DistribuidorService@executar');
Core\Router::post('/token', '\Core\OAuth2\TokenServer@executar');

$request = Request::createFromGlobals();
$response = new Response();
Router::rotear($request,$response);
$response->send();

