<?php

namespace Core\Ferramentas;

/**
 * Description of Instancias
 *
 * @author cleoj
 */
class Instancias {

    public static function instanciaDoCaminho($caminho, array $parametros = []) {
        if (!class_exists($caminho)) {
            //erro
        }
        //return new $caminho;
        extract($parametros);

        $instancia = null;
        $parametrosString = $parametros !== [] ? '$' . implode(', $', array_keys($parametros)) : '';
        eval('$instancia = new ' . $caminho . '(' . $parametrosString . ');');
        if ($instancia === null) {
            //erro
        }
        return $instancia;
    }

}
