<?php

namespace Core\Migracao\Abstrato;

/**
 * Description of AMigracao
 *
 * @author cleoj
 */
abstract class AMigracao {

    abstract function atualizar();

    abstract function reverter();
}
