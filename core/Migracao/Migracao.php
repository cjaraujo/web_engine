<?php

namespace Core\Migracao;

/**
 * Description of Migracao
 *
 * @author cleoj
 */
class Migracao {

    private $repositorio;

    public function __construct() {
        $this->criarTabelaMigracao();
        $this->repositorio = new Repositorio\Repositorio();
    }

    public function executar() {
        $pastas = glob(APP_DIR . '\core\*', GLOB_ONLYDIR);

        foreach ($pastas as $pasta) {
            $pasta = explode(DIRECTORY_SEPARATOR, $pasta);
            $pasta = end($pasta);
            $migracoes = glob(APP_DIR . '\core\\' . $pasta . '\Migracao\*');
            foreach($migracoes as $migracao) {
                $nameSpace = trim($migracao, '.php');
                $nameSpace = str_replace([APP_DIR, 'core'], ['', 'Core'], $nameSpace);

                if (!($nameSpace instanceof Abstrato\AMigracao)) {
                    //erro
                }
                try {

                    $instancia = new $nameSpace;
                    $instancia->atualizar();
                } catch (\Exception $e) {
                    
                }
            }
        }
    }

    private function criarTabelaMigracao() {
        $criadorDeTabelas = \Core\BancoDeDados\BancoDeDados::criadorDeTabelas();
        $criadorDeTabelas->tabela('migracao');
        $criadorDeTabelas->addInt('mi_id', 11, false);
        $criadorDeTabelas->autoIncremento('mi_id');
        $criadorDeTabelas->addVarchar('mi_modulo', 32, false);
        $criadorDeTabelas->addVarchar('mi_arquivo', 128, false);
        $criadorDeTabelas->addSmallint('mi_finalizado', 1, false);
        $criadorDeTabelas->addSmallint('mi_revertido', 1, false);
        $criadorDeTabelas->addDateTime('mi_data_finalizado');
        $criadorDeTabelas->addDateTime('mi_data_revertido');
        $criadorDeTabelas->chavePrimaria('mi_id');
        $criadorDeTabelas->index('mi_modulo');
        $criadorDeTabelas->criar();
    }

    public function migrarPorModulo($modulo) {
        $this->repositorio->whereModulo('=', $modulo);
        $this->repositorio->db()->abrirGrupo();
        $this->repositorio->whereFinalizado('=', 1);
        $this->repositorio->orWhereRevertido('=', 1);
        $this->repositorio->db()->fecharGrupo();
        $atualizacoes = $this->repositorio->buscar();
        $arquivosJaArualizados = $atualizacoes->getArrayDeCampo('arquivo');
        $arquivos = glob(APP_DIR . '\modulos\\' . $modulo . '\Migracao\*');
        foreach ($arquivos as $arquivo) {
            $segmentos = explode('\\', $arquivo);
            $nomeArquivo = end($segmentos);
            $nomeArquivo = trim($nomeArquivo, '.php');
            if (in_array($nomeArquivo, $arquivosJaArualizados)) {
                continue;
            }
            $nameSpace = trim($arquivo, '.php');
            $nameSpace = str_replace([APP_DIR, 'modulos'], ['', 'Modulos'], $nameSpace);

            if (!($nameSpace instanceof Abstrato\AMigracao)) {
                //erro
            }
            try {
                $instancia = new $nameSpace;
                $instancia->atualizar();
            } catch (\Exception $e) {
                
            }
            $entidade = new Repositorio\Entidade($this->repositorio);
            $entidade->modulo = $modulo;
            $entidade->arquivo = $nomeArquivo;
            $entidade->finalizado = 1;
            $entidade->revertido = 0;
            $entidade->data_finalizado = date('Y-m-d H:i:s');
            $entidade->inserir();
        }
    }

}
