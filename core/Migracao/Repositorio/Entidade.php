<?php

namespace Core\Migracao\Repositorio;

/**
 * Description of Entidade
 *
 * @author cleoj
 */
class Entidade extends \Core\Repositorio\Entidade {

    protected $campos = ['id', 'modulo', 'arquivo', 'finalizado', 'revertido', 'data_finalizado', 'data_revertido'];
    protected $classeRepositorio = Repositorio::class;

}
