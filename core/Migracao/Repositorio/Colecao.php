<?php

namespace Core\Migracao\Repositorio;

/**
 * Description of Colecao
 *
 * @author cleoj
 */
class Colecao extends \Core\Repositorio\Colecao {

    protected $campos = ['id', 'modulo', 'arquivo', 'finalizado', 'revertido', 'data_finalizado', 'data_revertido'];

}
