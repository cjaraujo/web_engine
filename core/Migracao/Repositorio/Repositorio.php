<?php

namespace Core\Migracao\Repositorio;

/**
 * Description of Repositorio
 *
 * @author cleoj
 */
class Repositorio extends \Core\Repositorio\ARepositorio {

    protected $tabela = 'migracao';
    protected $prefixo = 'mi_';
    protected $chavePrimaria = 'id';
    protected $campos = ['id', 'modulo', 'arquivo', 'finalizado', 'revertido', 'data_finalizado', 'data_revertido'];
    protected $classeEntidade = Entidade::class;
    protected $classeColecao = Colecao::class;

}
