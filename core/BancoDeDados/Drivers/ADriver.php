<?php
namespace Core\BancoDeDados\Drivers;
/**
 * Description of Adriver
 *
 * @author cleoj
 */
abstract class ADriver {

    public function __construct() {
        $this->conectar();
    }

    abstract function conectar();

    abstract function executar($query);

    /**
     * @return AResultado
     */
    abstract function resultado();
    

    abstract function escape($string,$quotes = false);
    abstract function ultimoIdInserido();
}
