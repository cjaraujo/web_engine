<?php

namespace Core\BancoDeDados\Drivers;

/**
 * Description of Resultado
 *
 * @author cleoj
 */
abstract class AResultado {

    abstract function linha();

    abstract function resultado();
}
