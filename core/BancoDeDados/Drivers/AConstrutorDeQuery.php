<?php

namespace Core\BancoDeDados\Drivers;

/**
 * Description of AConstrutorDeQuery
 *
 * @author cleoj
 */
abstract class AConstrutorDeQuery {

    protected static $OPERADOR_OU = "ou";
    protected static $OPERADOR_E = "e";
    protected static $OPERADOR_NAO = "nao";
    protected static $ISOLADOR_ABRIR = '(';
    protected static $ISOLADOR_FECHAR = ')';
    protected static $DIRECAO_ASCEDENTE = 'asc';
    protected static $DIRECAO_DESCEDENTE = 'desc';
    protected $select = [];
    protected $joins = [];
    protected $wheres = [];
    protected $havings = [];
    protected $orderBys = [];
    protected $groupsBys = [];
    protected $tabela = '';
    protected $limit;
    private $ultimaQueryExecutada;

    /**
     *
     * @var ADriver 
     */
    protected $driver;

    /**
     *
     * @var string 
     */
    protected $query = '';

    public function __construct(ADriver $driver = null) {
        $this->driver = $driver;
    }

    abstract function montarQuery();

    abstract function montarInserir($tabela, array $dados);

    public function inserir($tabela, array $dados) {

        $this->montarInserir($tabela, $dados);
        $this->driver->executar($this->query);
        $this->ultimaQueryExecutada = $this->query;
        $this->resetarQuery();
        return $this->driver->ultimoIdInserido();
    }

    abstract function montarSubstiruir($tabela, array $dados);

    public function substituir($tabela, array $dados) {
        $this->substituir($tabela, $dados);
        $this->driver->executar($this->query);
        $this->ultimaQueryExecutada = $this->query;
        $this->resetarQuery();
        return $this->driver->ultimoIdInserido();
    }

    abstract function montarAtualizar($tabela, array $dados);

    public function atualizar($tabela, array $dados) {
        $this->montarAtualizar($tabela, $dados);
        $this->driver->executar($this->query);
        $this->ultimaQueryExecutada = $this->query;
        $this->resetarQuery();
    }

    abstract function montarDelete($tabela);

    public function deletar($tabela) {
        $this->montarDelete($tabela);
        $this->driver->executar($this->query);
        $this->ultimaQueryExecutada = $this->query;
        $this->resetarQuery();
    }

    /**
     * 
     * @param string $tabela
     * @return \Core\BancoDeDados\Entidades\Resultado
     */
    public function get($tabela = null) {
        if ($tabela) {
            $this->from($tabela);
        }
        $this->montarQuery();
        $this->driver->executar($this->query);
        $this->ultimaQueryExecutada = $this->query;
        $this->resetarQuery();
        return $this->driver->resultado();
    }

    protected function resetarQuery() {
        $this->select = [];
        $this->joins = [];
        $this->wheres = [];
        $this->havings = [];
        $this->orderBys = [];
        $this->groupsBys = [];
        $this->tabela = '';
        $this->query = '';
    }

    public function ultimaQueryExecutada() {
        $ultimaQuery = $this->ultimaQueryExecutada;
        $this->ultimaQueryExecutada = '';
        return $ultimaQuery;
    }

    /**
     * 
     * @return string
     */
    public function getSelectConstruido() {
        $this->montarQuery();
        $retorno = $this->query;
        $this->resetarQuery();
        return $retorno;
    }

    /**
     * 
     * @return ADriver
     */
    public function getDriver() {
        return $this->driver;
    }

    /**
     * 
     * @param array $selects
     * @param bool $quotes
     * @param bool $resetarSelect
     * @return AConstrutorDeQuery
     */
    public function select(array $selects, $quotes = true, $resetarSelect = false) {
        if ($resetarSelect) {
            $this->select = [];
        }
        foreach ($selects as $select) {
            $this->select[] = ['campo' => $select, 'quotes' => $quotes];
        }
        return $this;
    }

    /**
     * 
     * @param string $tabela
     * @param string $on
     * @param string $lado
     * @param bool $quotes
     * @return AConstrutorDeQuery
     */
    public function join($tabela, $on, $lado = 'INNER', $quotes = true) {
        $this->joins[] = ['tabela' => $tabela, 'on' => $on, 'lado' => $lado, 'quotes' => $quotes];
        return $this;
    }

    /**
     * 
     * @param string $campo
     * @param string $operador
     * @param string|array $valor
     * @param bool $quotes
     * @return AConstrutorDeQuery
     */
    public function where($campo, $operador = null, $valor = null, $quotes = true) {
        $this->wheres[] = ['campo' => $campo, 'operador' => $operador, 'valor' => $valor, 'quotes' => $quotes, "tipo_logico" => self::$OPERADOR_E];
        return $this;
    }

    /**
     * 
     * @param string $campo
     * @param string $operador
     * @param string|array $valor
     * @param bool $quotes
     * @return AConstrutorDeQuery
     */
    public function orWhere($campo, $operador = null, $valor = null, $quotes = true) {
        $this->wheres[] = ['campo' => $campo, 'operador' => $operador, 'valor' => $valor, 'quotes' => $quotes, "tipo_logico" => self::$OPERADOR_OU];
        return $this;
    }

    /**
     * 
     * @param string $campo
     * @param string $operador
     * @param string|array $valor
     * @param bool $quotes
     * @return AConstrutorDeQuery
     */
    public function having($campo, $operador = null, $valor = null, $quotes = true) {
        $this->havings[] = ['campo' => $campo, 'operador' => $operador, 'valor' => $valor, 'quotes' => $quotes, "tipo_logico" => self::$OPERADOR_E];
        return $this;
    }

    /**
     * 
     * @param string $campo
     * @param string $operador
     * @param string|array $valor
     * @param bool $quotes
     * @return AConstrutorDeQuery
     */
    public function orHaving($campo, $operador = null, $valor = null, $quotes = true) {
        $this->havings[] = ['campo' => $campo, 'operador' => $operador, 'valor' => $valor, 'quotes' => $quotes, "tipo_logico" => self::$OPERADOR_OU];
        return $this;
    }

    /**
     * 
     * @param string $tabela
     */
    public function from($tabela) {
        $this->tabela = $tabela;
        return $this;
    }

    /**
     * 
     * @return AConstrutorDeQuery
     */
    public function abrirGrupo() {
        $this->wheres[] = ['isolador' => self::$ISOLADOR_ABRIR];
        return $this;
    }

    /**
     * 
     * @return AConstrutorDeQuery
     */
    public function fecharGrupo() {
        $this->wheres[] = ['isolador' => self::$ISOLADOR_FECHAR];
        return $this;
    }

    /**
     * 
     * @return AConstrutorDeQuery
     */
    public function abrirGrupoOu() {
        $this->wheres[] = ['isolador' => self::$OPERADOR_OU . ' ' . self::$ISOLADOR_ABRIR];
        return $this;
    }

    /**
     * 
     * @return AConstrutorDeQuery
     */
    public function abrirGrupoNao() {
        $this->wheres[] = ['isolador' => self::$OPERADOR_NAO . ' ' . self::$ISOLADOR_ABRIR];
        return $this;
    }

    /**
     * 
     * @return AConstrutorDeQuery
     */
    public function abrirGrupoOuNao() {
        $this->wheres[] = ['isolador' => self::$OPERADOR_OU . ' ' . self::$OPERADOR_NAO . ' ' . self::$ISOLADOR_ABRIR];
        return $this;
    }

    /**
     * 
     * @return AConstrutorDeQuery
     */
    public function abrirGrupoHaving() {
        $this->havings[] = ['isolador' => self::$ISOLADOR_ABRIR];
        return $this;
    }

    /**
     * 
     * @return AConstrutorDeQuery
     */
    public function fecharGrupoHaving() {
        $this->havings[] = ['isolador' => self::$ISOLADOR_FECHAR];
        return $this;
    }

    /**
     * 
     * @return AConstrutorDeQuery
     */
    public function abrirGrupoHavingOu() {
        $this->havings[] = ['isolador' => self::$OPERADOR_OU . ' ' . self::$ISOLADOR_ABRIR];
        return $this;
    }

    /**
     * 
     * @return AConstrutorDeQuery
     */
    public function abrirGrupoHavingNao() {
        $this->havings[] = ['isolador' => self::$OPERADOR_NAO . ' ' . self::$ISOLADOR_ABRIR];
        return $this;
    }

    /**
     * 
     * @return AConstrutorDeQuery
     */
    public function abrirGrupoHavingOuNao() {
        $this->havings[] = ['isolador' => self::$OPERADOR_OU . ' ' . self::$OPERADOR_NAO . ' ' . self::$ISOLADOR_ABRIR];
        return $this;
    }
    
    public function orderBy($coluna, $direcao = null) {
        if ($direcao === null) {
            $direcao = self::$DIRECAO_ASCEDENTE;
        }
        $this->orderBys[] = ['coluna' => $coluna, 'direcao' => $direcao];
        return $this;
    }

    public function groupBy($coluna) {
        $this->groupsBys[] = $coluna;
        return $this;
    }

    public function limit(int $quantidade, int $start = null) {
        $this->limit = [];
        $this->limit[] = $quantidade;
        if ($start !== null) {
            $this->limit[] = $start;
        }
        return $this;
    }

}
