<?php

namespace Core\BancoDeDados\Drivers;

/**
 * Description of ACriadorDeTabela
 *
 * @author cleoj
 */
abstract class ACriadorDeTabela {

    protected static $TIPO_INT = 'INTEGER';
    protected static $TIPO_VARCHAR = 'VARCHAR';
    protected static $TIPO_SMALLINT = 'TINYINT';
    protected static $TIPO_TEXT = 'TEXT';
    protected static $TIPO_DOUBLE = 'DOUBLE';
    protected static $TIPO_DATETIME = 'DATETIME';
    /**
     *
     * @var ADriver 
     */
    protected $driver;
    protected $colunas;
    protected $chavePrimaria;
    protected $tabela;
    protected $query;
    protected $indexes;

    public function __construct(ADriver $driver) {
        $this->driver = $driver;
    }

    public function tabela(string $tabela) {
        $this->tabela = $tabela;
        return $this;
    }

    abstract function montarQuery();

    private function limparQuery() {
        $this->colunas = [];
        $this->chavePrimaria = [];
        $this->tabela = '';
        $this->query = '';
    }

    public function criar() {
        $this->montarQuery();
        $this->driver->executar($this->query);
        $this->limparQuery();
        //return logs
    }

    public function addInt(string $nomeDaColuna, int $tamanho, bool $null = true) {
        $this->colunas[$nomeDaColuna] = ['nome' => $nomeDaColuna, 'tipo' => self::$TIPO_INT, 'tamanho' => $tamanho, 'null' => $null];
        return $this;
    }
    public function autoIncremento(string $nomeDaColuna){
        $this->colunas[$nomeDaColuna]['auto_incremento'] = true;
        return $this;
    }

    public function addVarchar(string $nomeDaColuna, int $tamanho, bool $null = true) {
        $this->colunas[$nomeDaColuna] = ['nome' => $nomeDaColuna, 'tipo' => self::$TIPO_VARCHAR, 'tamanho' => $tamanho, 'null' => $null];
        return $this;
    }

    public function addSmallint(string $nomeDaColuna, int $tamanho, bool $null = true) {
        $this->colunas[$nomeDaColuna] = ['nome' => $nomeDaColuna, 'tipo' => self::$TIPO_SMALLINT, 'tamanho' => $tamanho, 'null' => $null];
        return $this;
    }

    public function addText(string $nomeDaColuna, bool $null = true) {
        $this->colunas[$nomeDaColuna] = ['nome' => $nomeDaColuna, 'tipo' => self::$TIPO_TEXT, 'tamanho' => false, 'null' => $null];
        return $this;
    }

    public function addDouble(string $nomeDaColuna, string $tamanho, bool $null = true) {
        $this->colunas[$nomeDaColuna] = ['nome' => $nomeDaColuna, 'tipo' => self::$TIPO_DOUBLE, 'tamanho' => $tamanho, 'null' => $null];
        return $this;
    }
    public function addDateTime(string $nomeDaColuna, bool $null = true) {
        $this->colunas[$nomeDaColuna] = ['nome' => $nomeDaColuna, 'tipo' => self::$TIPO_DATETIME, 'tamanho' => false, 'null' => $null];
        return $this;
    }

    public function coluna(string $nomeDaColuna, string $tipo, int $tamanho, bool $null = true) {
        $this->colunas[$nomeDaColuna] = ['nome' => $nomeDaColuna, 'tipo' => $tipo, 'tamanho' => $tamanho, 'null' => $null];
        return $this;
    }

    public function valorPadrao($nomeDaColuna, $valorPadrao) {
        $this->colunas[$nomeDaColuna]['valor_padrao'] = $valorPadrao;
        return $this;
    }

    public function chavePrimaria(string $chavesPrimarias) {
        $this->chavePrimaria[] = $chavesPrimarias;
        return $this;
    }

    public function index(string $index) {
        $this->indexes[] = $index;
        return $this;
    }

}
