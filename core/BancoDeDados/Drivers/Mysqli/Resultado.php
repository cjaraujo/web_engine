<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\BancoDeDados\Drivers\Mysqli;

/**
 * Description of Resultado
 *
 * @author cleoj
 */
class Resultado extends \Core\BancoDeDados\Drivers\AResultado {
    private $driver;
    
    public function __construct(Driver $driver) {
        $this->driver = $driver;
    }
    public function linha() {
        return (object) $this->driver->resultado->fetch_assoc();
    }

    public function resultado() {
        $resultado = null;
         while ($row = $this->driver->resultado->fetch_assoc()) {
            $resultado[] = (object) $row;
        }
        return $resultado;
    }

}