<?php

namespace Core\BancoDeDados\Drivers\Mysqli;

use Core\BancoDeDados\Drivers\ADriver;
use Core\Config\BancoDeDados;
use mysqli;

/**
 * Description of Driver
 *
 * @author cleoj
 */
class Driver extends ADriver {

    private $mysql;
    public $resultado;

    public function conectar() {
        $this->mysql = new mysqli(BancoDeDados::getHost(), BancoDeDados::getUsername(), BancoDeDados::getPassword(), BancoDeDados::getDatabase());
    }

    public function executar($query) {

        $this->resultado = $this->mysql->query($query);
    }

    /**
     * 
     * @return Resultado
     */
    public function resultado() {
        return new Resultado($this);
    }

    public function escape($string, $quotes = false) {
     
    }
    public function ultimoIdInserido(){
        return $this->mysql->insert_id;
    }
}
