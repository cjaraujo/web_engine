<?php

namespace Core\BancoDeDados\Drivers\Mysqli;

/**
 * Description of ConstrutorDeQuery
 *
 * @author cleoj
 */
class ConstrutorDeQuery extends \Core\BancoDeDados\Drivers\AConstrutorDeQuery {

    public function __construct() {
        parent::$ISOLADOR_ABRIR = '(';
        parent::$ISOLADOR_FECHAR = ')';
        parent::$OPERADOR_E = 'AND';
        parent::$OPERADOR_OU = 'OR';
        parent::$OPERADOR_NAO = 'NOT';
        parent::__construct(new Driver());
    }

    public function montarQuery() {
        $this->montarSelect();
        $this->montarJoins();
        $this->montarWheres();
    }

    private function montarSelect() {
        $this->query .= "SELECT ";
        foreach ($this->select as $select) {
            if ($select['quotes']) {
                $select = $this->quote($select['campo']);
                $this->query .= "$select, ";
                continue;
            }
            $this->query .= "$select, ";
        }
        $this->query = trim($this->query, ', ');

        $this->query .= " FROM";
        $this->query .= " {$this->quote($this->tabela)} ";
    }

    private function montarJoins() {
        foreach ($this->joins as $join) {
            if ($join['quotes']) {
                $this->query .= "{$join['lado']} JOIN {$this->quote($join['tabela'])} ON {$this->quote($join['on'])} ";
                continue;
            }
            $this->query .= "{$join['lado']} {$join['tabela']} ON {$join['on']} ";
        }
    }

    private function quote($string) {
        $string = strtolower($string);
        $array = (array) explode(' ', $string);
        foreach ($array as &$dado) {
            $dado = trim($dado);
            $this->palavrasReservadas($dado);
            $dado = str_replace('.', '`.`', $dado);
        }
        return implode(' ', $array);
    }

    private function palavrasReservadas(&$string) {
        switch ($string) {
            case 'as':
                $string = 'AS';
                break;
            case '=':
                break;
            case '>=':
                break;
            case '<=':
                break;
            case 'like':
                $string = 'LIKE';
                break;
            case 'and':
                $string = 'AND';
                break;
            case 'or':
                $string = 'OR';
                break;
            case 'not':
                $string = 'NOT';
                break;
            case 'in':
                $string = 'IN';
                break;
            default :
                $string = "`$string`";
                break;
        }
    }

    private function montarWheres() {
        if (!$this->wheres) {
            return;
        }
        $this->query .= "WHERE ";
        $primeiraCondicao = true;
        foreach ($this->wheres as $where) {
            if (isset($where['isolador'])) {
                if (!$primeiraCondicao && $where['isolador'] == self::$ISOLADOR_ABRIR) {
                    $this->query .= self::$OPERADOR_E . " ";
                }
                $this->query .= "{$where['isolador']} ";
                $primeiraCondicao = true;
                continue;
            }

            if ($where['quotes']) {
                if (!$primeiraCondicao) {
                    $this->query .= "{$where['tipo_logico']} ";
                }
                $primeiraCondicao = false;
                $this->query .= "{$this->quote($where['campo'])} ";
                if ($where['operador']) {
                    $this->query .= "{$where['operador']} ";
                }
                if (isset($where['valor'])) {
                    if (is_array($where['valor'])) {
                        $valores = "( ";
                        foreach ($where['valor'] as $valor) {
                            $valores .= "'$valor', ";
                        }
                        $valores = trim($valores, ', ');
                        $valores .= ")";
                        $this->query .= "{$valores} ";
                        continue;
                    }
                    $this->query .= "'{$where['valor']}' ";
                }
                continue;
            }
            if (!$primeiraCondicao) {
                $this->query .= "{$where['tipo_logico']} ";
            }
            $primeiraCondicao = false;
            $this->query .= "{$where['campo']} ";
            if ($where['operador']) {
                $this->query .= "{$where['operador']} ";
            }
            if (isset($where['valor'])) {
                if (is_array($where['valor'])) {
                    $valores = "( ";
                    foreach ($where['valor'] as $valor) {
                        $valores .= "{$valor}, ";
                    }
                    $valores = trim($valores, ', ');
                    $valores .= ")";
                    $this->query .= "{$valores} ";
                    continue;
                }
                $this->query .= "{$where['valor']} ";
            }
            continue;
        }
    }

    public function montarDelete($tabela) {
        if (!$this->tabela) {
            $this->tabela = $tabela;
        }
        $this->query .= "DELETE " . $this->quote($tabela) . " FROM " . $this->quote($this->tabela);
        $this->montarJoins();
        $this->montarWheres();
    }

    public function montarInserir($tabela, array $dados) {
        $campos = implode(' ', array_keys($dados));
        $campos  = $this->quote($campos);
        $campos = str_replace(' ', ', ', $campos);
        $valores = implode("', '", $dados);
        $this->query = "INSERT INTO ".$this->quote($tabela)." ($campos) VALUES ('$valores')";
    }

    public function montarSubstiruir($tabela, array $dados) {
        
    }

    public function montarAtualizar($tabela, array $dados) {
        
    }

}
