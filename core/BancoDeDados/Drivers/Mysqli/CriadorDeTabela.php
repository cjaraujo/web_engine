<?php

namespace Core\BancoDeDados\Drivers\Mysqli;

/**
 * Description of CriadorDeTabela
 *
 * @author cleoj
 */
class CriadorDeTabela extends \Core\BancoDeDados\Drivers\ACriadorDeTabela {

    public function __construct() {
        parent::__construct(new Driver());
    }

    public function montarQuery() {
        $this->query .= "CREATE TABLE IF NOT EXISTS {$this->tabela} (";
        foreach ($this->colunas as $coluna) {
            $nulo = $coluna['null'] || isset($coluna['auto_incremento']) ? '' : ' NOT NULL';
            $coluna['tipo'] = $coluna['tamanho'] !== false ? $coluna['tipo'] . "({$coluna['tamanho']})" : $coluna['tipo'];
            $autoIncremento = isset($coluna['auto_incremento']) && $coluna['auto_incremento'] ? ' AUTO_INCREMENT' : '';
            $this->query .= " `{$coluna['nome']}` {$coluna['tipo']}{$nulo}{$autoIncremento},";
        }
        if ($this->chavePrimaria) {
            $chavePrimaria = implode('`, `', $this->chavePrimaria);
            $this->query .= " PRIMARY KEY (`$chavePrimaria`),";
        }
        foreach ((array) $this->indexes as $index) {
            $this->query .= " INDEX `index_$index` (`$index`),";
        }
        $this->query = trim($this->query, ',');
        $this->query .= ')  ENGINE=INNODB;';
    }

}
