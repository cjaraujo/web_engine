<?php

namespace Core\BancoDeDados;

/**
 * Description of BancoDeDados
 *
 * @author cleoj
 */
class BancoDeDados {

    private static $INSTANCIA = null;

    /**
     * 
     * @param \Drivers\AConstrutorDeQuery $novaInstancia
     * @return type
     */
    public static function get($novaInstancia = false) {
        if ($novaInstancia) {
            return self::$INSTANCIA = new Drivers\Mysqli\ConstrutorDeQuery();
        }
        if (self::$INSTANCIA != null) {
            return self::$INSTANCIA;
        }
        return self::$INSTANCIA = new Drivers\Mysqli\ConstrutorDeQuery();
    }

    /**
     * 
     * @return \Core\BancoDeDados\Drivers\ACriadorDeTabela
     */
    public static function criadorDeTabelas() {
        return new Drivers\Mysqli\CriadorDeTabela();
    }

}
