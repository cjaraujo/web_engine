<?php

namespace Core\Config;

/**
 * Configuração do banco de dados.
 * @author marcos
 */
class BancoDeDados {
    
    private static $driver = 'mysql';
    private static $host = 'localhost';
    private static $database = 'teste';
    private static $username = 'root';
    private static $password = '';
    private static $charset = 'utf8';
    private static $collation = 'utf8_unicode_ci';
    private static $prefix = '';
    private static $strict = '';
    private static $engine = 'INNODB';
   
    static function getDriver() {
        return self::$driver;
    }

    static function getHost() {
        return self::$host;
    }

    static function getDatabase() {
        return self::$database;
    }

    static function getUsername() {
        return self::$username;
    }

    static function getPassword() {
        return self::$password;
    }

    static function getCharset() {
        return self::$charset;
    }

    static function getCollation() {
        return self::$collation;
    }

    static function getPrefix() {
        return self::$prefix;
    }

    static function getStrict() {
        return self::$strict;
    }
    static function getEngine(){
        return self::$engine;
    }
}
