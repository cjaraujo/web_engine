<?php

namespace Core;

/**
 * Startup da aplicação
 * @author Marcos
 */
class Bootstrap
{

    public static function start($diretorioAplicacao)
    {
        define('APP_DIR', $diretorioAplicacao);
        ini_set('error_reporting', E_ALL);
        ini_set("display_errors", 1);
        date_default_timezone_set('America/Sao_Paulo');
       // new Request();
    }
}