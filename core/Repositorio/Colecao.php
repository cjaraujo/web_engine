<?php

namespace Core\Repositorio;

/**
 * Description of Colecao
 *
 * @author cleoj
 */
abstract class Colecao extends \Core\Abstratos\AColecao{

    protected $campos = [];
    
    public function getArrayDeCampo($campo){
        if(!in_array($campo, $this->campos)){
            //erro
        }
        $retorno = [];
        foreach ($this->dados as $dado){
            $retorno[] = $dado->{$campo};
        }
        return $retorno;
    }
}
