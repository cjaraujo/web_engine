<?php

namespace Core\Repositorio;

/**
 * Description of Entidade
 *
 * @author cleoj
 */
abstract class Entidade extends \Core\Abstratos\AEntidade {

    protected $repositorio;
    protected $classeRepositorio;

    public function __construct(ARepositorio $repositorio = null) {
        if ($repositorio === null) {
            $this->repositorio = \Core\Ferramentas\Instancias::instanciaDoCaminho($this->classeRepositorio);
            return;
        }
        $this->repositorio = $repositorio;
    }

    public function substituir() {
        $this->repositorio->substituir($this);
    }

    public function inserir() {
        $this->repositorio->inserir($this);
    }

    public function atualizar() {
        $this->repositorio->atualizar($this);
    }

    public function deletar() {
       $this->repositorio->deletar($this);
    }

}
