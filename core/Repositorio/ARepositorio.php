<?php

namespace Core\Repositorio;

/**
 * Description of ARepositorio
 *
 * @author cleoj
 */
abstract class ARepositorio {

    protected $campos = [];
    protected $prefixo = '';
    protected $tabela = '';
    protected $chavePrimaria = '';
    protected $classeEntidade = '';
    protected $classeColecao = '';

    /**
     *
     * @var \Core\BancoDeDados\Drivers\AConstrutorDeQuery 
     */
    private $db;

    public function __construct() {
        $this->db = \Core\BancoDeDados\BancoDeDados::get(true);
        $this->montarSelect();
    }

    public function __call($nome, $argumentos) {
        if (strpos($nome, 'where') === 0) {
            $nome = str_replace('where', '', $nome);
            $campo = \Core\Ferramentas\Conversao::pascalCaseToUnderscore($nome);
            $this->db->where($this->prefixo . $campo, (isset($argumentos[0]) ? $argumentos[0] : null), (isset($argumentos[1]) ? $argumentos[1] : null));
            return $this;
        }
        if (strpos($nome, 'having') === 0) {
            $nome = str_replace('having', '', $nome);
            $campo = \Core\Ferramentas\Conversao::pascalCaseToUnderscore($nome);
            $this->db->having($this->prefixo . $campo, (isset($argumentos[0]) ? $argumentos[0] : null), (isset($argumentos[1]) ? $argumentos[1] : null));
            return $this;
        }
        if (strpos($nome, 'or') === 0) {
            $nome = str_replace('or', '', $nome);
            $nome[0] = strtolower($nome[0]);
            if (strpos($nome, 'where') === 0) {
                $nome = str_replace('where', '', $nome);
                $campo = \Core\Ferramentas\Conversao::pascalCaseToUnderscore($nome);
                $this->db->orWhere($this->prefixo . $campo, (isset($argumentos[0]) ? $argumentos[0] : null), (isset($argumentos[1]) ? $argumentos[1] : null));
                return $this;
            }
            if (strpos($nome, 'having') === 0) {
                $nome = str_replace('having', '', $nome);
                $campo = \Core\Ferramentas\Conversao::pascalCaseToUnderscore($nome);
                $this->db->orHaving($this->prefixo . $campo, (isset($argumentos[0]) ? $argumentos[0] : null), (isset($argumentos[1]) ? $argumentos[1] : null));
                return $this;
            }
        }
        //throw erro 
    }

    /**
     * 
     * @return \Core\BancoDeDados\Drivers\AConstrutorDeQuery
     */
    public function db(): \Core\BancoDeDados\Drivers\AConstrutorDeQuery {
        return $this->db;
    }

    public function primeiro() {
        
        $linha = $this->db->get($this->tabela)->linha();
        $entidade = \Core\Ferramentas\Instancias::instanciaDoCaminho($this->classeEntidade, ['repositorio' => $this]);
        if (!$linha) {
            return $entidade;
        }
        foreach ($this->campos as $campo) {
            $entidade->{$campo} = $linha->{$this->prefixo . $campo};
        }
        return $entidade;
    }

    public function buscar() {
        $resultado = $this->db->get($this->tabela)->resultado();
        $retorno = \Core\Ferramentas\Instancias::instanciaDoCaminho($this->classeColecao);
        if (!$resultado) {
            return $retorno;
        }

        foreach ($resultado as $linha) {
            $entidade = \Core\Ferramentas\Instancias::instanciaDoCaminho($this->classeEntidade, ['repositorio' => $this]);
            foreach ($this->campos as $campo) {
                $entidade->{$campo} = $linha->{$this->prefixo . $campo};
            }
            $retorno->adicionar($entidade);
        }
        return $retorno;
    }

    protected function montarSelect() {
        $select = [];
        foreach ($this->campos as $campo) {
            $select[] = $this->prefixo . $campo;
        }
        $this->db->select($select);
    }

    public function salvar(Entidade $entidade) {
        $dados = [];
        foreach ($this->campos as $campo) {
            $dados[$this->prefixo . $campo] = $entidade->{$campo};
        }
        return $this->db->inserir($this->tabela, $dados);
    }

    public function substituir(Entidade $entidade) {
        $dados = [];
        foreach ($this->campos as $campo) {
            $dados[$this->prefixo . $campo] = $entidade->{$campo};
        }
        return $this->db->substituir($this->tabela, $dados);
    }

    public function inserir(Entidade $entidade) {
        $dados = [];
        foreach ($this->campos as $campo) {
            if ($entidade->{$campo} === null) {
                continue;
            }
            $dados[$this->prefixo . $campo] = $entidade->{$campo};
        }
        return $this->db->inserir($this->tabela, $dados);
    }

    public function atualizar(Entidade $entidade) {
        $dados = [];
        foreach ($this->campos as $campo) {
            $dados[$this->prefixo . $campo] = $entidade->{$campo};
        }
        return $this->db->atualizar($this->tabela, $dados);
    }

    public function deletar(Entidade $entidade) {
        foreach ($this->campos as $campo) {
            if ($entidade->{$campo} === null) {
                $this->db->where($this->prefixo . $campo, 'IS NULL');
                continue;
            }
            $this->db->where($this->prefixo . $campo, '=', $entidade->{$campo});
        }
        return $this->db->deletar($this->tabela);
    }

}
