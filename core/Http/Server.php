<?php

namespace Core\Http;

/**
 * Variaveis de ambiente do servidor
 * @author marcos
 */
class Server
{
    private static $documentRoot;
    private static $remoteAddr;
    private static $remotePort;
    private static $serverSoftware;
    private static $serverProtocol;
    private static $serverName;
    private static $serverPort;
    private static $requestUri;
    private static $requestMethod;
    private static $scriptName;
    private static $scriptFilename;
    private static $phpSelf;
    private static $httpHost;
    private static $httpConnection;
    private static $httpUpgradeInsecureRequest;
    private static $httpUserAgent;
    private static $httpAccept;
    private static $httpAcceptEncoding;
    private static $httpAcceptLanguage;
    private static $requestTimeFloat;
    private static $requestTime;

    public function __construct()
    {
        self::$documentRoot               = $_SERVER['DOCUMENT_ROOT'];
        self::$remoteAddr                 = $_SERVER['REMOTE_ADDR'];
        self::$remotePort                 = $_SERVER["REMOTE_PORT"];
        self::$serverSoftware             = $_SERVER["SERVER_SOFTWARE"];
        self::$serverProtocol             = $_SERVER["SERVER_PROTOCOL"];
        self::$serverName                 = $_SERVER["SERVER_NAME"];
        self::$serverPort                 = $_SERVER["SERVER_PORT"];
        self::$requestUri                 = $_SERVER["REQUEST_URI"];
        self::$requestMethod              = $_SERVER["REQUEST_METHOD"];
        self::$scriptName                 = $_SERVER["SCRIPT_NAME"];
        self::$scriptFilename             = $_SERVER["SCRIPT_FILENAME"];
        self::$phpSelf                    = $_SERVER["PHP_SELF"];
        self::$httpHost                   = $_SERVER["HTTP_HOST"];
        self::$httpConnection             = $_SERVER["HTTP_CONNECTION"] ?? null;
        self::$httpUpgradeInsecureRequest = $_SERVER["HTTP_UPGRADE_INSECURE_REQUESTS"]
                ?? null;
        self::$httpUserAgent              = $_SERVER["HTTP_USER_AGENT"];
        self::$httpAccept                 = $_SERVER["HTTP_ACCEPT"];
        self::$httpAcceptEncoding         = $_SERVER["HTTP_ACCEPT_ENCODING"] ?? null;
        self::$httpAcceptLanguage         = $_SERVER["HTTP_ACCEPT_LANGUAGE"] ?? null;
        self::$requestTimeFloat           = $_SERVER["REQUEST_TIME_FLOAT"];
        self::$requestTime                = $_SERVER["REQUEST_TIME"];
    }

    public static function getDocumentRoot()
    {
        return self::$documentRoot;
    }

    public static function getRemoteAddr()
    {
        return self::$remoteAddr;
    }

    public static function getRemotePort()
    {
        return self::$remotePort;
    }

    public static function getServerSoftware()
    {
        return self::$serverSoftware;
    }

    public static function getServerProtocol()
    {
        return self::$serverProtocol;
    }

    public static function getServerName()
    {
        return self::$serverName;
    }

    public static function getServerPort()
    {
        return self::$serverPort;
    }

    public static function getRequestUri()
    {
        return self::$requestUri;
    }

    public static function getRequestMethod()
    {
        return self::$requestMethod;
    }

    public static function getScriptName()
    {
        return self::$scriptName;
    }

    public static function getScriptFilename()
    {
        return self::$scriptFilename;
    }

    public static function getPhpSelf()
    {
        return self::$phpSelf;
    }

    public static function getHttpHost()
    {
        return self::$httpHost;
    }

    public static function getHttpConnection()
    {
        return self::$httpConnection;
    }

    public static function getHttpUpgradeInsecureRequest()
    {
        return self::$httpUpgradeInsecureRequest;
    }

    public static function getHttpUserAgent()
    {
        return self::$httpUserAgent;
    }

    public static function getHttpAccept()
    {
        return self::$httpAccept;
    }

    public static function getHttpAcceptEncoding()
    {
        return self::$httpAcceptEncoding;
    }

    public static function getHttpAcceptLanguage()
    {
        return self::$httpAcceptLanguage;
    }

    public static function getRequestTimeFloat()
    {
        return self::$requestTimeFloat;
    }

    public static function getRequestTime()
    {
        return self::$requestTime;
    }
}