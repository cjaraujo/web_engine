<?php

namespace Core\Http\Execption;

/**
 * Description of SuspiciousOperationException
 * @author marcos
 */
class SuspiciousOperationException extends \UnexpectedValueException implements RequestExceptionInterface
{

}