<?php

namespace Core\Http\Execption;

/**
 * Description of ConflictingHeadersException
 * @author marcos
 */
class ConflictingHeadersException extends \UnexpectedValueException implements RequestExceptionInterface
{

}