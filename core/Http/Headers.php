<?php

namespace Core\Http;

/**
 * Cabeçalho do pacote http
 * @author marcos
 */
class Headers
{
    private static $headers = array();
     /**
     * @param array $server
     * @return array
     */
    private static function getHeaders()
    {
        if(!empty(self::$headers)){
            return self::$headers;
        }
        self::$headers = array();
        $server = $_SERVER;
        foreach ($server as $key => $value) {
            if (0 === strpos($key, 'HTTP_')) {
                self::$headers[substr($key, 5)] = $value;
            }
            // CONTENT_* are not prefixed with HTTP_
            elseif (in_array($key, array('CONTENT_LENGTH', 'CONTENT_MD5', 'CONTENT_TYPE'))) {
                self::$headers[$key] = $value;
            }
        }

        if (isset($server['PHP_AUTH_USER'])) {
            self::$headers['PHP_AUTH_USER'] = $server['PHP_AUTH_USER'];
            self::$headers['PHP_AUTH_PW'] = isset($server['PHP_AUTH_PW']) ? $server['PHP_AUTH_PW'] : '';
        } else {
            $authorizationHeader = null;
            if (isset($server['HTTP_AUTHORIZATION'])) {
                $authorizationHeader = $server['HTTP_AUTHORIZATION'];
            } elseif (isset($server['REDIRECT_HTTP_AUTHORIZATION'])) {
                $authorizationHeader = $server['REDIRECT_HTTP_AUTHORIZATION'];
            } elseif (function_exists('apache_request_headers')) {
                $requestHeaders = (array) apache_request_headers();

                // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
                $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));

                if (isset($requestHeaders['Authorization'])) {
                    $authorizationHeader = trim($requestHeaders['Authorization']);
                }
            }

            if (null !== $authorizationHeader) {
                self::$headers['AUTHORIZATION'] = $authorizationHeader;
                // Decode AUTHORIZATION header into PHP_AUTH_USER and PHP_AUTH_PW when authorization header is basic
                if (0 === stripos($authorizationHeader, 'basic')) {
                    $exploded = explode(':', base64_decode(substr($authorizationHeader, 6)));
                    if (count($exploded) == 2) {
                        list(self::$headers['PHP_AUTH_USER'], self::$headers['PHP_AUTH_PW']) = $exploded;
                    }
                }
            }
        }

        // PHP_AUTH_USER/PHP_AUTH_PW
        if (isset(self::$headers['PHP_AUTH_USER'])) {
            self::$headers['AUTHORIZATION'] = 'Basic '.base64_encode(self::$headers['PHP_AUTH_USER'].':'.self::$headers['PHP_AUTH_PW']);
        }

        return self::$headers;
    }
}