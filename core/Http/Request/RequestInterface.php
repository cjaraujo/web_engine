<?php

namespace Core;

/**
 * @author marcos
 */
interface RequestInterface
{

    /**
     * @param string $nome
     * @param mixed  $padrao
     * @return mixed
     */
    public function query($nome, $padrao = null);

    /**
     * @param string $nome
     * @param mixed  $padrao
     * @return mixed
     */
    public function request($nome, $padrao = null);

    /**
     * @param string $nome
     * @param mixed  $padrao
     * @return mixed
     */
    public function getServer($nome, $padrao = null);

    /**
     * @param string $nome
     * @param mixed  $padrao
     * @return mixed
     */
    public function getHeaders($nome, $padrao = null);

    /**
     * @return mixed
     */
    public function getAllQueryParameters();
}