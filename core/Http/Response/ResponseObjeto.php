<?php

namespace Core\Http\Response;

/**
 * Objeto de dados da resposta
 * @author marcos
 */
abstract class ResponseObjeto extends StatusCode
{
    /**
     * @var ResponseHeader $headers
     */
    protected $headers;

    /**
     * @var string
     */
    protected $conteudo;

    /**
     * @var string
     */
    protected $versao;

    /**
     * @var string
     */
    protected $charset;

    /**
     * @var int
     */
    protected $statusCode;

    /**
     * @var string
     */
    protected $statusTexto;

    public function getHeaders()
    {
        return $this->headers;
    }

    public function getConteudo()
    {
        return $this->conteudo;
    }

    public function getVersao(): string
    {
        return $this->versao;
    }

    public function getCharset(): string
    {
        return $this->charset;
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function getStatusTexto(): string
    {
        return $this->statusTexto;
    }

    public function setHeaders($headers)
    {
        $this->headers = $headers;
        return $this;
    }

    public function setConteudo($conteudo)
    {
        if (null !== $conteudo && !\is_string($conteudo) && !is_numeric($conteudo) && !\is_callable(array($conteudo, '__toString'))) {
            throw new \UnexpectedValueException(sprintf('The Response content must be a string or object implementing __toString(), "%s" given.', \gettype($conteudo)));
        }
        $this->conteudo = (string) $conteudo;
        return $this;
    }

    public function setVersao(string $versao)
    {
        $this->versao = $versao;
        return $this;
    }

    public function setCharset(string $charset)
    {
        $this->charset = $charset;
        return $this;
    }

    /**
     * Inseri o codigo da resposta
     * @var int $codigo
     * @var string|boolean $texto
     * @return $this
     * @throws \InvalidArgumentException quando o status informado é invalido
     */
    public function setStatusCode(int $codigo, $texto = null)
    {
        $this->statusCode = $codigo;
        if ($this->isInvalid()) {
            throw new \InvalidArgumentException(sprintf('The HTTP status code "%s" is not valid.', $codigo));
        }
        if (null === $texto) {
            $this->statusTexto = isset(self::$statusTextos[$codigo]) ? self::$statusTextos[$codigo] : 'unknown status';
            return $this;
        }
        if (false === $texto) {
            $this->statusTexto = '';
            return $this;
        }
        $this->statusTexto = $texto;
        return $this;
    }

    public function setStatusTexto(string $statusTexto)
    {
        $this->statusTexto = $statusTexto;
        return $this;
    }

    /**
     * Verifica se o codigo informado é valido.
     * @return bool
     */
    public function isInvalid(): bool
    {
        return $this->statusCode < 100 || $this->statusCode >= 600;
    }
}