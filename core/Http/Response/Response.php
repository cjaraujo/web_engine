<?php

namespace Core\Http\Response;

class Response extends ResponseObjeto implements ResponseInterface
{

    /**
     * @throws \InvalidArgumentException Quando o codigo informado não é valido.
     */
    public function __construct($conteudo = '', int $status = 200, array $headers = array('Content-Type'=> 'text/html'))
    {
        $this->headers = new ResponseHeader($headers);
        $this->setConteudo($conteudo);
        $this->setStatusCode($status);
        $this->setVersao('1.0');
    }

    public function __toString()
    {
        return
        sprintf('<pre>HTTP/%s %s %s', $this->versao, $this->statusCode, $this->statusTexto)."\r\n".
        $this->headers."\r\n".
        $this->getConteudo()."</pre>";
    }

    /**
     * Criando um retorno statico
     * @param mixed $conteudo O conteudo da resposta
     * @param int   $status  codigo da resposta
     * @param array $headers Headers da resposta
     * @return static
     */
    public static function create($conteudo = '', int $status = 200, array $headers = array())
    {
        return new static($conteudo, $status, $headers);
    }

    /**
     * Retorna json
     * @param array $dados
     * @return type
     */
    public static function json(array $conteudo = array(), int $status = 200)
    {
        $conteudo = json_encode($conteudo);
        return new static($conteudo, $status, ['Content-Type' => 'application/json']);
    }

    /**
     * Envia o cabeçalho HTTP e o conteúdo
     * @return $this
     */
    public function send()
    {
        $this->sendHeaders();
        $this->sendContent();
        if (\function_exists('fastcgi_finish_request')) {
            fastcgi_finish_request();
        } elseif (!\in_array(\PHP_SAPI, array('cli', 'phpdbg'), true)) {
            static::closeOutputBuffers(0, true);
        }
        return $this;
    }

    /**
     * Envia o cabeçalho HTTP.
     * @return $this
     */
    private function sendHeaders()
    {
        // headers have already been sent by the developer
        if (headers_sent()) {
            return $this;
        }
        // headers
        foreach ($this->headers->allPreserveCaseWithoutCookies() as $name => $values) {
            foreach ($values as $value) {
                header($name.': '.$value, false, $this->statusCode);
            }
        }

        foreach ($this->headers->getCookies() as $cookie) {
            header('Set-Cookie: '.$cookie->getName().strstr($cookie, '='), false, $this->statusCode);
        }

        header(sprintf('HTTP/%s %s %s', $this->versao, $this->statusCode, $this->statusTexto), true, $this->statusCode);
        return $this;
    }

    /**
     * Escreve o conteudo.
     * @return $this
     */
    private function sendContent()
    {
        echo $this->conteudo;
        return $this;
    }

    /**
     * Limpa/libera buffer
     */
    private static function closeOutputBuffers(int $targetLevel, bool $flush)
    {
        $status = ob_get_status(true);
        $level  = \count($status);
        $flags  = PHP_OUTPUT_HANDLER_REMOVABLE | ($flush ? PHP_OUTPUT_HANDLER_FLUSHABLE : PHP_OUTPUT_HANDLER_CLEANABLE);
        while ($level-- > $targetLevel && ($s      = $status[$level]) && (!isset($s['del']) ? !isset($s['flags']) || ($s['flags'] & $flags) === $flags : $s['del'])) {
            if ($flush) {
                ob_end_flush();
            } else {
                ob_end_clean();
            }
        }
    }
}