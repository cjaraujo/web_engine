<?php

namespace Core\UnidadesDeMedida;

/**
 * Grandeza Massa
 * @author marcos
 */
abstract class AMassa extends AUnidadesDeMedida
{
    public function __construct(float $quantidade = 0)
    {
        parent::__construct($quantidade);
        $this->grandeza = 'Massa';
    }

    /**
     * Conversão entre unidades da grandeza massa.
     * @param \Core\UnidadesDeMedida\AMassa $unidade
     * @return type
     */
    public function conversao(AMassa $unidade)
    {
        return parent::conversao($unidade);
    }
}