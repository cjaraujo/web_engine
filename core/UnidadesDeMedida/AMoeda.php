<?php

namespace Core\UnidadesDeMedida;

/**
 * Grandeza Moeda
 * @author marcos
 */
abstract class AMoeda extends AUnidadesDeMedida
{
    public function __construct(float $quantidade)
    {
        parent::__construct($quantidade);
        $this->grandeza = 'Moeda';
    }

    /**
     * Conversão entre unidades da grandeza moeda.
     * @param \Core\UnidadesDeMedida\AMoeda $unidade
     * @return type
     */
    public function conversao(AMoeda $unidade)
    {
        return parent::conversao($unidade);
    }
}