<?php

namespace Core\UnidadesDeMedida;

/**
 * Grandeza Comprimento
 * @author marcos
 */
abstract class AComprimento extends AUnidadesDeMedida
{
    public function __construct(float $quantidade)
    {
        parent::__construct($quantidade);
        $this->grandeza = 'Comprimento';
    }

    /**
     * Conversão entre unidades da grandeza comprimento.
     * @param \Core\UnidadesDeMedida\AComprimento $unidade
     * @return type
     */
    public function conversao(AComprimento $unidade)
    {
        return parent::conversao($unidade);
    }
}