<?php

namespace Core\UnidadesDeMedida;

/**
 * Classe base para unidades de medida
 * @author marcos
 */
abstract class AUnidadesDeMedida
{
    protected $nome;
    protected $sigla;
    protected $grandeza;
    protected $conversao;
    protected $quantidade;

    /**
     * Cria objeto de unidade de medida
     * @param float $quantidade
     */
    public function __construct(float $quantidade = 0)
    {
        $this->quantidade = $quantidade;
    }

    public function __toString()
    {
        return (string) $this->quantidade.$this->sigla;
    }

    /**
     * Cria um objeto de unidade de medida
     * @param type $quantidade
     * @return \static
     */
    public static function criar($quantidade)
    {
        return new static($quantidade);
    }

    /**
     * Realiza conversão entre tipos de unidade de mesma grandeza.
     * @param \Core\UnidadesDeMedida\AUnidadesDeMedida $unidade
     * @return \Core\UnidadesDeMedida\AUnidadesDeMedida
     */
    public function conversao(AUnidadesDeMedida $unidade)
    {
        $unidade->quantidade = ($this->quantidade * $this->conversao) / $unidade->conversao;
        return $unidade;
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function getSigla()
    {
        return $this->sigla;
    }

    public function getConversao()
    {
        return $this->conversao;
    }

    public function getQuantidade()
    {
        return $this->quantidade;
    }
}