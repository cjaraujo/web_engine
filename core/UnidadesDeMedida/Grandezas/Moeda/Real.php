<?php

use Core\UnidadesDeMedida\AMoeda;

namespace Core\UnidadesDeMedida\Grandezas\Moeda;

/**
 * Unidade de medida real da grandeza Moeda
 * @author marcos
 */
class Real extends AMoeda
{

    public function __construct(float $quantidade)
    {
        parent::__construct($quantidade);
        $this->nome      = 'real';
        $this->sigla     = 'R$';
        $this->conversao = 1;
    }
}