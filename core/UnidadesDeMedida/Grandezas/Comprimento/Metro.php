<?php

namespace Core\UnidadesDeMedida\Grandezas\Comprimento;

use Core\UnidadesDeMedida\AComprimento;

/**
 * Unidade de medida Metro da grandeza Comprimento
 * @author marcos
 */
class Metro extends AComprimento
{

    public function __construct(float $quantidade)
    {
        parent::__construct($quantidade);
        $this->nome      = 'quilograma';
        $this->sigla     = 'kg';
        $this->conversao = 1;
    }
}