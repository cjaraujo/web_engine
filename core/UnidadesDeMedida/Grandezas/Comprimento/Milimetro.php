<?php

namespace Core\UnidadesDeMedida\Grandezas\Comprimento;

use Core\UnidadesDeMedida\AComprimento;

/**
 * Unidade de medida Milimetro da grandeza Comprimento
 * @author marcos
 */
class Milimetro extends AComprimento
{

    public function __construct(float $quantidade)
    {
        parent::__construct($quantidade);
        $this->nome      = 'milimetro';
        $this->sigla     = 'mm';
        $this->conversao = 1000;
    }
}