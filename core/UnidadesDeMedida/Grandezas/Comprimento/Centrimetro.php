<?php

namespace Core\UnidadesDeMedida\Grandezas\Comprimento;

use Core\UnidadesDeMedida\AComprimento;

/**
 * Unidade de medida Centimetro da grandeza Comprimento
 * @author marcos
 */
class Centimetro extends AComprimento
{

    public function __construct(float $quantidade)
    {
        parent::__construct($quantidade);
        $this->nome      = 'centimetro';
        $this->sigla     = 'cm';
        $this->conversao = 100;
    }
}