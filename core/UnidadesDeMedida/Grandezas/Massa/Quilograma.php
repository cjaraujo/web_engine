<?php

namespace Core\UnidadesDeMedida\Grandezas\Massa;

use Core\UnidadesDeMedida\AMassa;

/**
 * Unidade de medida Quilograma da grandeza Massa
 * @author marcos
 */
class Quilograma extends AMassa
{
    public function __construct(float $quantidade = 0)
    {
        parent::__construct($quantidade);
        $this->nome = 'quilograma';
        $this->sigla = 'kg';
        $this->conversao = 1;
    }
    
}