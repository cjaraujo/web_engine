<?php

namespace Core\UnidadesDeMedida\Grandezas\Massa;

use Core\UnidadesDeMedida\AMassa;

/**
 * Unidade de medida Miligrama da grandeza Massa
 * @author marcos
 */
class Miligrama extends AMassa
{
    public function __construct(float $quantidade = 0)
    {
        parent::__construct($quantidade);
        $this->nome = 'miligrama';
        $this->sigla = 'mg';
        $this->conversao = 0.000001;
    }
    
}