<?php

namespace Core\UnidadesDeMedida\Grandezas\Massa;

use Core\UnidadesDeMedida\AMassa;

/**
 * Unidade de medida Grama da grandeza Massa
 * @author marcos
 */
class Grama extends AMassa
{
    public function __construct(float $quantidade = 0)
    {
        parent::__construct($quantidade);
        $this->nome = 'grama';
        $this->sigla = 'g';
        $this->conversao = 1000;
    }
    
}