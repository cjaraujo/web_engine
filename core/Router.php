<?php

namespace Core;

use \Core\Http\Request\Request;
use \Core\Http\Response\Response;

/**
 * Gerencia o roteamento do sistema.
 */
class Router
{
    /** @var Request */
    private static $request;

    /** @var Response */
    private static $response;
    private static $uri;
    private static $rotas = [
        'GET' => [],
        'POST' => [],
        'PUT' => [],
        'PATCH' => [],
        'DELETE' => [],
        'OPTIONS' => []
    ];

    public static function find(string $uri): Action
    {
        $metodo = self::$request->getMethod();
        if (!array_key_exists($uri, self::$rotas[$metodo])) {
            throw new \Exception('Rota não encontrada!', 404);
        }
        return self::$rotas[$metodo][$uri];
    }

    public static function get(string $uri, string $action = null)
    {
        self::addRoute('GET', $uri, $action);
    }

    public static function post(string $uri, string $action = null)
    {
        self::addRoute('POST', $uri, $action);
    }

    public static function put(string $uri, string $action = null)
    {
        self::addRoute('PUT', $uri, $action);
    }

    public static function patch(string $uri, string $action = null)
    {
        self::addRoute('PATCH', $uri, $action);
    }

    public static function delete(string $uri, string $action = null)
    {
        self::addRoute('DELETE', $uri, $action);
    }

    public static function options(string $uri, string $action = null)
    {
        self::addRoute('OPTIONS', $uri, $action);
    }

    private static function addRoute(string $verb, $uri, $action)
    {
        $controllerMethod         = explode('@', $action);
        $actionController         = new Action($controllerMethod[0], $controllerMethod[1]);
        self::$rotas[$verb][$uri] = $actionController;
    }

    /**
     *
     * @param Request $request
     * @param Response $response
     */
    public static function rotear(Request $request, Response $response)
    {
        self::$request  = $request;
        self::$response = $response;
        self::$uri      = urldecode(parse_url($request->getUri(), PHP_URL_PATH));

        try {
            $action     = Router::find(self::$uri);
            $controller = $action->getController();
            $metodo     = $action->getMethod();

            $controllerInstance = new $controller();

            if (!method_exists($controllerInstance, $metodo)) {
                exit('404 not found controller or method!');
            }
            $controllerInstance->$metodo($request, $response);
        } catch (\Exception $ex) {
            Response::json(array('mensagem' => $ex->getMessage(), 'statusCode' => $ex->getCode()), $ex->getCode())
            ->send();
            exit;
        }
    }
}