<?php

namespace Core;

/**
 * Ação a ser executada em uma requisição
 * @author marcos
 */
class Action
{
    private $controller;
    private $method;
    private $parameters;

    public function __construct(string $controller, string $method = 'index',$parameters = [])
    {
        $this->controller = $controller;
        $this->method     = $method;
        $this->parameters = $parameters;
    }

    public function getController()
    {
        return $this->controller;
    }

    public function getMethod()
    {
        return $this->method;
    }

    public function getParameters()
    {
        return $this->parameters;
    }
}