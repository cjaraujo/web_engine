<?php

namespace Core\Abstratos;

/**
 * Description of AServico
 *
 * @author cleoj
 */
abstract class AServico {

    protected $dependencias = [];

    function getDependencias() {
        return $this->dependencias;
    }

}
