<?php

namespace Core\Abstratos;

/**
 * Description of AColecao
 *
 * @author cleoj
 */
abstract class AColecao {

    protected $dados = [];
    public function adicionar($entidade, $chave = null) {
        if ($chave === null) {
            $this->dados[] = $entidade;
            return $this;
        }
        $this->dados[$chave] = $entidade;
        return $this;
    }

    /**
     * 
     * @param string $chave
     * @return Entidade
     */
    public function obter($chave) {
        if (!isset($this->dados[$chave])) {
            //erro
        }
        return $this->dados[$chave];
    }

    public function quantidade() {
        return (int) count($this->dados);
    }

    /**
     * 
     * @return Entidade[]
     */
    public function obterArray() {
        return $this->dados;
    }

    public function estaVazio() {
        return (bool) empty($this->dados);
    }

    public function limpar() {
        $this->dados = [];
    }

}
