<?php

namespace Core\Abstratos;

/**
 * Description of AEntidade
 *
 * @author cleoj
 */
abstract class AEntidade {

    protected $campos = [];
    private $valores = [];

    public function __get($name) {
        if (!in_array($name, $this->campos)) {
            //erro
        }
        if (!isset($this->valores[$name])) {
            return null;
        }
        return $this->valores[$name];
    }

    public function __set($name, $value) {
        if (!in_array($name, $this->campos)) {
            //erro
        }
        $this->valores[$name] = $value;
    }

    public function obterArray() {
        return $this->valores;
    }

}
