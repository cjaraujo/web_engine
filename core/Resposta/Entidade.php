<?php

namespace Core\Resposta;

/**
 * Description of Entidade
 *
 * @author cleoj
 */
class Entidade extends \Core\Abstratos\AEntidade{
    protected $select;
    
    public function __construct(array $select) {
        $this->select = $select;
    }
}
