<?php

namespace Core\Resposta;

/**
 * Description of Colecao
 *
 * @author cleoj
 */
class Colecao extends \Core\Abstratos\AColecao {

    protected $quantidadeTotal;
    protected $quantidadePorPagina;
    protected $quantidadePagina;
    protected $paginaAual;
    protected $select;
    protected $campos;
    protected $queryString;
    protected $queryStringValores;
    protected $queryStringOperadores;

    public function __construct(array $queryStringValores = [], array $queryStringOperadores = [], array $select = [],int $quantidadePorPagina = 25, int $paginaAtual = 0) {
        $this->queryStringValores = $queryStringValores;
        $this->queryStringOperadores = $queryStringOperadores;
        $this->select = $select;
        $this->paginaAual = $paginaAtual;
        $this->quantidadePorPagina = $quantidadePorPagina;
    }

    public function getQuantidadeTotal() {
        return $this->quantidadeTotal;
    }

    public function getQuantidadePagina() {
        return $this->quantidadePagina;
    }
    
    function getQueryString() {
        return $this->queryString;
    }

    function getQueryStringValores(string $campo) {
        if(!isset($this->queryStringValores[$campo])){
            return null;
        }
        return $this->queryStringValores[$campo];
    }

    function getQueryStringOperadores(string $campo) {
        if(!isset($this->queryStringOperadores[$campo])){
            return null;
        }
        return $this->queryStringOperadores[$campo];
    }

    
    public function getSelect() {
        if(!$this->select){
            return $this->campos;
        }
        foreach ($this->select as $select){
            if(!in_array($select, $this->campos)){
                //erro
            }
        }
        return $this->select;
    }

    public function getPaginaAual() {
        return $this->paginaAual;
    }

    public function setQuantidadeTotal($quantidadeTotal) {
        $this->quantidadeTotal = $quantidadeTotal;
        return $this;
    }
    function setQuantidadePagina($quantidadePagina) {
        $this->quantidadePagina = $quantidadePagina;
        return $this;
    }


}
