<?php

namespace Core\Resposta;

/**
 * Description of Repositorio
 *
 * @author cleoj
 */
class Repositorio extends \Core\Repositorio\ARepositorio {

    public function popularColecao(Colecao $colecao) {
        $this->carregarSelectQueryString($colecao);
        $this->carregarQueryStrings($colecao);
        
    }

    private function carregarLimit(Colecao $colecao) {
        $this->db()->limit($colecao->getQuantidadePagina(), $colecao->getPaginaAual() * $colecao->getQuantidadePagina());
    }

    private function carregarSelectQueryString(Colecao $colecao) {
        $this->campos = $colecao->getSelect();
        $select = [];
        foreach ($this->campos as $campo) {
            $select[] = $this->prefixo . $campo;
        }
        $this->db->select($select,true,true);
    }

    private function carregarQueryStrings(Colecao $colecao) {
        foreach ($colecao->getQueryString() as $queryString) {
            $valor = $colecao->getQueryStringValores($queryString);
            if ($valor === null) {
                continue;
            }
            $operador = $colecao->getQueryStringOperadores($queryString) !== null ? $colecao->getQueryStringOperadores($queryString) : '=';
            $this->db()->where($queryString, $operador, $valor);
        }
    }
    private function carregarQuantidadeTotal(Colecao $colecao){
        $db = clone $this->db();
        $db->from($this->tabela);
        $queryMontada = $db->getSelectConstruido();
        $db->select(['count(1) quantidade']);
        $resultado = $db->get("($queryMontada) temp")->linha();
        if(isset($resultado->quantidade)){
            $colecao->setQuantidadeTotal($resultado->quantidade);
            return;
        }
        $colecao->setQuantidadeTotal(0);
    }

}
