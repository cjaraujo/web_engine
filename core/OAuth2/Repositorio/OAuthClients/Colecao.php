<?php

namespace Core\OAuth2\Repositorio\OAuthClients;

/**
 * Description of Colecao
 * @author Marcos
 */
class Colecao extends \Core\Repositorio\Colecao
{
    protected $campos = ['id', 'client_id', 'client_secret', 'redirect_uri', 'grant_types', 'scope', 'user_id'];
}