<?php

namespace Core\OAuth2\Repositorio\OAuthClients;

/**
 * Description of Repositorio
 * @author Marcos
 */
class Repositorio extends \Core\Repositorio\ARepositorio{
    protected $tabela = 'oauth_clients';
    protected $prefixo = 'oc_';
    protected $chavePrimaria = 'id';
    protected $campos = ['id', 'client_id', 'client_secret', 'redirect_uri', 'grant_types', 'scope', 'user_id'];
    protected $classeEntidade = Entidade::class;
    protected $classeColecao = Colecao::class;
}
