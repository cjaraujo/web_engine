<?php

namespace Core\OAuth2\Repositorio\OAuthClients;

/**
 * Description of Entidade
 * @author Marcos
 */
class Entidade extends \Core\Repositorio\Entidade{
    protected $campos = ['id', 'client_id', 'client_secret', 'redirect_uri', 'grant_types', 'scope', 'user_id'];
    protected $classeRepositorio = Repositorio::class;
}
