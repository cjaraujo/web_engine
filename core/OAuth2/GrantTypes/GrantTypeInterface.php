<?php

namespace Core\OAuth2\GrantTypes;

use Core\Http\Request\Request;
use Core\Http\Response\Response;

/**
 *
 * @author marcos
 */
interface GrantTypeInterface
{

    /**
     * Identifica o tipo autenticação
     */
    public function getIdentificador(): string;

    /**
     *
     * @param Request $requisicao
     * @param Response $resposta
     */
    public function validateRequest(Request $requisicao, Response $resposta);

    /**
     * Get client id
     * @return mixed
     */
    public function getClientId();

    /**
     * Get user id
     * @return mixed
     */
    public function getUserId();

    /**
     * Get scope
     * @return string|null
     */
    public function getScope();

    /**
     * Cria o token de acesso.
     * @param AccessTokenInterface $accessToken
     * @param mixed                $clientId
     * @param mixed                $userId
     * @param string               $scope
     * @return array
     */
    public function criarTokenDeAcesso(AccessTokenInterface $accessToken, $clientId, $userId, $scope);
}