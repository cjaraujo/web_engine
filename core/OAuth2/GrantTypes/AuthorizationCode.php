<?php

namespace Core\OAuth2\GrantTypes;

use Core\Http\Request\Request;
use Core\Http\Response\Response;
use Exception;

/**
 * GrantType
 * @author marcos
 */
class AuthorizationCode implements GrantTypeInterface
{
    protected $codigoDeAutenticacao;

    /**
     * @return string
     */
    public function getIdentificador(): string
    {
        return 'authorization_code';
    }

    /**
     * Validate the OAuth request
     *
     * @param Request $request
     * @param Response $response
     * @return bool
     * @throws Exception
     */
    public function validateRequest(Request $request, Response $response)
    {
        if (!$request->get('code')) {
            $response->setConteudo('Missing parameter: "code" is required');
            $response->setStatusCode(400);
            return false;
        }

        $code     = $request->get('code');
        if (!$authCode = $this->storage->getAuthorizationCode($code)) {
            $response->setConteudo('Authorization code doesn\'t exist or is invalid for the client');
            $response->setStatusCode(400);
            return false;
        }

        if (isset($authCode['redirect_uri']) && $authCode['redirect_uri']) {
            if (!$request->request('redirect_uri') || urldecode($request->request('redirect_uri')) != urldecode($authCode['redirect_uri'])) {
                $response->setConteudo('The redirect URI is missing or do not match');
                $response->setStatusCode(400);
                return false;
            }
        }

        if (!isset($authCode['expires']) || $authCode["expires"] < time()) {
            $response->setConteudo('The authorization code has expired');
            $response->setStatusCode(400);
            return false;
        }

        $this->authCode = $authCode;

        return true;
    }

    public function criarTokenDeAcesso(AccessTokenInterface $accessToken, $clientId, $userId, $scope): array
    {
        
    }

    public function getClientId()
    {

    }

    public function getScope()
    {
        
    }

    public function getUserId()
    {

    }
}