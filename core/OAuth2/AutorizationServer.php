<?php

namespace Core\OAuth2;

use \Core\Http\Request\Request;
use \Core\Http\Response\Response;

/**
 * Servidor de autorização OAUTH2
 * @author marcos
 */
class AutorizationServer {

    /**
     * @param Request $request
     * @param Response $response
     */
    public static function executar(Request $request, Response $response) {

        if (!$request->get('response_type')) {
            $response->setConteudo('Missing parameter: "response_type" is required');
            $response->setStatusCode(400);
            return false;
        }
        if (!$request->get('client_id')) {
            $response->setConteudo('Missing parameter: "client_id" is required');
            $response->setStatusCode(400);
            return false;
        }
        if (!$request->get('redirect_uri')) {
            $response->setConteudo('Missing parameter: "redirect_uri" is required');
            $response->setStatusCode(400);
            return false;
        }
        if (!$request->get('scope')) {
            $response->setConteudo('Missing parameter: "scope" is required');
            $response->setStatusCode(400);
            return false;
        }
        if (!$request->get('state')) {
            $response->setConteudo('Missing parameter: "state" is required');
            $response->setStatusCode(400);
            return false;
        }

        $repositorio = new Repositorio\OAuthClients\Repositorio();
        $resultado = $repositorio->whereClientId('=', $request->get('client_id'))->primeiro();

        if (trim($resultado->redirect_uri) != trim($request->get('redirect_uri'))) {
            $response->setConteudo('Miss match "redirect_uri"');
            $response->setStatusCode(400);
            return false;
        }
        $code = self::criarSecret() ;
        $url = $redirectUrl = $resultado->redirect_uri . "?code={$code}&state={$request->get('state')}";
        header("Location: $url");
        exit();
    }
    
    public static function novoApp(){
        
    }

    private static function criarClient() {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
                // 32 bits for "time_low"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff),
                // 16 bits for "time_mid"
                mt_rand(0, 0xffff),
                // 16 bits for "time_hi_and_version",
                // four most significant bits holds version number 4
                mt_rand(0, 0x0fff) | 0x4000,
                // 16 bits, 8 bits for "clk_seq_hi_res",
                // 8 bits for "clk_seq_low",
                // two most significant bits holds zero and one for variant DCE1.1
                mt_rand(0, 0x3fff) | 0x8000,
                // 48 bits for "node"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    private static function criarSecret() {
        return bin2hex(random_bytes(32));
    }

}
