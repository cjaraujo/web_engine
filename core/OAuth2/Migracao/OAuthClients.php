<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\OAuth2\Migracao;

/**
 * Description of OAuthClients
 *
 * @author marcos
 */
class OAuthClients extends \Core\Migracao\Abstrato\AMigracao
{

    public function atualizar()
    {
        $criadorDeTabelas = \Core\BancoDeDados\BancoDeDados::criadorDeTabelas();
        $criadorDeTabelas->tabela('oauth_clients');
        $criadorDeTabelas->addInt('oc_id', 11, false);
        $criadorDeTabelas->autoIncremento('oc_id');
        $criadorDeTabelas->addVarchar('oc_client_id', 80, false);
        $criadorDeTabelas->addVarchar('oc_client_secret', 80, true);
        $criadorDeTabelas->addVarchar('oc_redirect_uri', 255, true);
        $criadorDeTabelas->addVarchar('oc_grant_types', 80, true);
        $criadorDeTabelas->addText('oc_scope', true);
        $criadorDeTabelas->addInt('oc_user_id',11, true);
        $criadorDeTabelas->chavePrimaria('oc_id');
        $criadorDeTabelas->index('oc_id');
        $criadorDeTabelas->index('oc_client_id');
        $criadorDeTabelas->criar();
    }

    public function reverter()
    {
    }
}