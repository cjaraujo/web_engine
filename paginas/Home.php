<?php
namespace Paginas;

class Home
{
    public function index()
    {
        $version = \Modulos\Layout\LayoutService::version();
        echo \Modulos\Layout\LayoutService::getLayout($version);
    }
}